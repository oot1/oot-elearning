package Controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import Bean.Item;
import Bean.KhoaHoc;
import Bean.NguoiDung;
import Bean.Order;
import Model.NguoiDungDb;
import Model.NguoiDungKhoaHocDb;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/Payment")
public class PayMent extends HttpServlet {
	private static final long serialVersionUID = 1L;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		NguoiDung nd = new NguoiDung();
		NguoiDungKhoaHocDb ndkh = new NguoiDungKhoaHocDb();
		List<Item> items = new ArrayList<Item>();
		HttpSession session = request.getSession();
		final String usernamemail = "ootelearning@gmail.com";
		final String passwordmail = "Anh670197";
		Properties prop = new Properties();
		prop.put("mail.smtp.host", "smtp.gmail.com");
		prop.put("mail.smtp.port", "587");
		prop.put("mail.smtp.auth", "true");
		prop.put("mail.smtp.socketFactory.port", "465");
		prop.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		String email;
		String IdCourses = "";
		int STT = 0;
		Session session1 = Session.getInstance(prop, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(usernamemail, passwordmail);
			}
		});

		String username = session.getAttribute("username") != null ? session.getAttribute("username").toString() : null;
		String password = session.getAttribute("password") != null ? session.getAttribute("password").toString() : null;
		Order order = (Order) session.getAttribute("order");
		try {
			nd = NguoiDungDb.getUser(username, password, 1);
			if (order != null && order.getItems().size() > 0) {
				for (Item kh : order.getItems()) {
					boolean check = false;
					for (KhoaHoc khoaHoc : ndkh.getListCourseOfUser(nd.getMaNguoiDung())) {
						if (kh.getKhoaHoc().getMaKhoaHoc() == khoaHoc.getMaKhoaHoc()) {
							check = true;
							break;
						} else {
							check = false;
						}
					}
					if (check == false) {
						ndkh.addNguoiDungKhoaHoc(kh.getKhoaHoc(), nd);
						STT++;
						String stt = Integer.toString(STT);
						IdCourses +=  stt + " - "+kh.getKhoaHoc().getTenKhoaHoc() + "\n";
					}
				}
				order.setItems(items);
				
				try {
					if (!IdCourses.equals("")) {
						email = nd.getEmail();
						Message message = new MimeMessage(session1);
						message.setFrom(new InternetAddress("ootelearning@gmail.com"));
						message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
						message.setSubject("Thank you for buying our courses");
						message.setText("Dear " + username
								+ "\n\n Thank you for buying our course \n Here is your Courses: \n" 
								 + IdCourses
								);
						Transport.send(message);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}

			}
			response.sendRedirect(request.getContextPath() + "/TrangChu");
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}

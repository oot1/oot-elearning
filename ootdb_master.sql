-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 16, 2020 at 11:17 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ootdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `danhmuc`
--

CREATE TABLE `danhmuc` (
  `maDanhMuc` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `tenDanhMuc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `moTa` varchar(500) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `danhmuc`
--

INSERT INTO `danhmuc` (`maDanhMuc`, `tenDanhMuc`, `moTa`) VALUES
('Business', 'Business', 'See why leading organizations choose Udemy for Business as their destination for employee learning'),
('Design', 'Design', 'Device design is the trend of today\'s youth, you should follow the trend !!'),
('Development', 'Development', 'This code will help you to learn how to develop a professional website.'),
('IT_Software', 'IT & Software', 'This course help you learn all about how to make and use moblie or desktop application'),
('Marketing', 'Marketing', 'If you want to be a good person in Marketing, this courses is the best choice.'),
('Photography', 'Photography', 'This course will help you to learn how to use some Photography\'s App competently.');

-- --------------------------------------------------------

--
-- Table structure for table `giaovien`
--

CREATE TABLE `giaovien` (
  `maGiaoVien` int(11) NOT NULL,
  `tenGiaoVien` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `moTa` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hinhAnh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `soLuongKH` int(11) NOT NULL,
  `soLuongHV` int(11) NOT NULL,
  `luotXem` int(11) NOT NULL,
  `xepHang` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `giaovien`
--

INSERT INTO `giaovien` (`maGiaoVien`, `tenGiaoVien`, `moTa`, `hinhAnh`, `soLuongKH`, `soLuongHV`, `luotXem`, `xepHang`) VALUES
(1, 'Alex Labby', 'Software Development Trainer', '16.jpg', 5, 2, 7863, 4.6),
(2, 'Laby Purce', 'Web Developer And Teacher', '17.jpg', 5, 3, 3489, 4.4),
(3, 'Rob Perci', 'Cinematographer / Photographer', '18.jpg', 5, 0, 4968, 4.3),
(4, 'Alex cen', 'Web Developer And Teacher', '19.jpg', 9, 7, 7812, 4.7),
(5, 'Avis Percival', 'Web Developer And Teacher', '20.jpg', 7, 5, 3489, 4.4),
(6, 'Phil Enine', 'Software Development Trainer', '21.jpg', 8, 3, 4968, 4.3),
(7, 'Scott Harris', 'Drawing, Digital Painting', '05.png', 14, 5, 3697, 4.3),
(8, 'Dale McManus', ' Landscape Photography', '22.jpg', 7, 7, 3698, 4);

-- --------------------------------------------------------

--
-- Table structure for table `khoahoc`
--

CREATE TABLE `khoahoc` (
  `maKhoaHoc` int(11) NOT NULL,
  `tenKhoaHoc` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `maDanhMuc` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `hinhAnh` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `soLuong` int(11) NOT NULL,
  `moTa` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `maGiaoVien` int(11) NOT NULL,
  `xepHang` float NOT NULL,
  `thanhTien` int(11) NOT NULL,
  `giamGia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `khoahoc`
--

INSERT INTO `khoahoc` (`maKhoaHoc`, `tenKhoaHoc`, `maDanhMuc`, `hinhAnh`, `soLuong`, `moTa`, `maGiaoVien`, `xepHang`, `thanhTien`, `giamGia`) VALUES
(1, 'The Complete Foundation Stock Trading Course', 'Business', 'Business_01.jpg', 3562, 'Learn To Trade The Stock Market by A Trading Firm CEO. Inc: Technical Analysis, Candlesticks, Stocks, Day Trading +++', 5, 4.5, 199, 60),
(2, 'The Complete Communication Skills Master Class for Life', 'Business', 'Business_02.jpg', 5135, 'Communication Skills for Persuasion, Assertiveness and all Business Communication Needs', 5, 4.3, 199, 68),
(3, 'An Entire MBA in 1 Course:Award Winning Business School Prof', 'Business', 'Business_03.jpg', 3256, '** #1 Best Selling Business Course! ** Everything You Need to Know About Business from Start-up to IPO', 1, 4.4, 250, 30),
(4, 'The Complete SQL Bootcamp 2020: Go from Zero to Hero', 'Business', 'Business_04.jpg', 1259, 'Become an expert at SQL!', 2, 4.1, 369, 22),
(5, 'The Complete Financial Analyst Course 2020', 'Business', 'Business_05.jpg', 4795, 'Excel, Accounting, Financial Statement Analysis, Business Analysis, Financial Math, PowerPoint: Everything is Included!', 3, 4.5, 236, 60),
(6, 'PMP Exam Prep Seminar - PMBOK Guide 6', 'Business', 'Business_06.jpg', 3650, 'PMP Exam Prep Seminar - Earn 35 PDUs by completing the entire PMP course', 6, 4.3, 399, 50),
(7, 'Tableau 2020 A-Z:Hands-On Tableau Training For Data Science!', 'Business', 'Business_07.jpg', 1256, 'Learn Tableau 2020 for Data Science step-by-step. Real-Life Data Analytics Exercises & Quizzes Included. Learn by doing!', 3, 4.4, 199, 30),
(8, 'Beginner to Pro in Excel: Financial Modeling and Valuation', 'Business', 'Business_08.jpg', 3257, 'Financial Modeling in Excel that would allow you to walk into a job and be a rockstar from day one!', 4, 4.3, 99, 60),
(9, 'The Complete Financial Analyst Training & Investing Course', 'Business', 'Business_09.jpg', 1256, 'Succeed as a Financial Analyst &Investor by Award Winning MBA Prof who worked @Goldman, in Hedge Funds & Venture Capital', 3, 4.2, 99, 30),
(10, 'The Ultimate Hands-On Hadoop - Tame your Big Data!', 'Business', 'Business_10.jpg', 6958, 'Hadoop tutorial with MapReduce, HDFS, Spark, Flink, Hive, HBase, MongoDB, Cassandra, Kafka + more! Over 25 technologies.', 1, 4.5, 399, 40),
(11, 'Writing With Flair: How To Become An Exceptional Writer', 'Business', 'Business_11.jpg', 3696, 'Ex-Wall Street Journal Editor Teaches How To Write With Confidence, Style & Impact', 2, 4.2, 599, 60),
(12, 'Become a Product Manager | Learn the Skills & Get the Job', 'Business', 'Business_12.jpg', 3700, 'The most complete course available on Product Management. 13+ hours of videos, activities, interviews, & more', 4, 4.3, 199, 10),
(13, 'The Business Intelligence Analyst Course 2020', 'Business', 'Business_13.jpg', 1145, 'The skills you need to become a BI Analyst - Statistics, Database theory, SQL, Tableau – Everything is included', 6, 4.4, 199, 30),
(14, 'Beginner to Pro in Excel: Financial Modeling and Valuation !', 'Business', 'Business_14.jpg', 6988, 'Financial Modeling in Excel that would allow you to walk into a job and be a rockstar from day one!', 5, 4.5, 199, 60),
(15, 'Graphic Design Masterclass - Learn GREAT Design', 'Design', 'Design_01.jpg', 6989, 'The Ultimate Graphic Design Course Which Covers Photoshop, Illustrator, InDesign,Design Theory, Branding and Logo Design', 1, 4.4, 199, 40),
(16, 'Maya for Beginners: Complete Guide to 3D Animation in Maya', 'Design', 'Design_02.jpg', 0, 'Learn everything you need for 3D animation in Autodesk Maya: Modeling, Texturing, Lighting, Rigging, Animation, Dynamics', 7, 4.4, 199, 40),
(17, 'Maya for Beginners: Complete Guide to 3D Animation in Maya !', 'Design', 'Design_02.jpg', 3874, 'Learn everything you need for 3D animation in Autodesk Maya: Modeling, Texturing, Lighting, Rigging, Animation, Dynamics', 7, 4.4, 99, 10),
(18, '3ds Max + V-Ray: 3ds Max PRO in 6 hrs', 'Design', 'Design_03.jpg', 3699, '3ds Max intro course: 3Ds Max and V-Ray for creating 3D architectural imagery, from beginner to advanced', 7, 4.3, 199, 40),
(19, 'Adobe Illustrator CC - Essentials Training Course', 'Design', 'Design_04.jpg', 3466, 'Learn Adobe Illustrator CC graphic design, logo design, and more with this in-depth, practical, easy-to-follow course!', 8, 4.5, 129, 40),
(20, 'Photorealistic Digital Painting From Beginner To Advanced.', 'Design', 'Design_05.jpg', 1459, 'Bring your imagination to life!', 7, 4.3, 129, 30),
(21, 'The Ultimate Drawing Course - Beginner to Advanced', 'Design', 'Design_06jpg.jpg', 1158, 'Learn the #1 most important building block of all art', 4, 4.3, 139, 40),
(22, 'Character Art School: Complete Character Drawing Course', 'Design', 'Design_07.jpg', 3679, 'Learn How to Draw People and Character Designs Professionally, Drawing for Animation, Comics, Cartoons, Games and More!', 1, 4.5, 199, 90),
(23, 'Complete Blender Creator: Learn 3D Modelling for Beginners', 'Design', 'Design_08.jpg', 6882, 'Use Blender to create beautiful 3D models for video games, 3D printing, house design etc. No prior knowledge required.', 8, 4.5, 269, 60),
(24, 'Illustrator CC 2020 MasterClass', 'Design', 'Design_09.jpg', 1459, 'Master Adobe Illustrator CC with this in-depth training for all levels', 7, 4.5, 199, 60),
(25, 'Ultimate Photoshop Training: From Beginner to Pro', 'Design', 'Design_10.jpg', 1789, 'Master Photoshop CC 2020 without any previous knowledge with this easy-to-follow course', 6, 4.2, 199, 20),
(26, 'After Effects CC 2020: Complete Course from Novice to Expert', 'Design', 'Design_11.jpg', 3357, 'Create stunning Motion Graphics, VFX Visual Effects & VFX Compositing with hands-on tutorials & 50+ practice projects.', 2, 4.4, 199, 10),
(27, 'Complete Python Bootcamp: Go from zero to hero in Python 3', 'Development', 'Development_01.jpg', 1148, 'Learn Python like a Professional! Start from the basics and go all the way to creating your own applications and games!', 1, 4.4, 199, 50),
(28, 'Python for Data Science and Machine Learning Bootcamp', 'Development', 'Development_02.jpg', 1257, 'Learn how to use NumPy, Pandas, Seaborn , Matplotlib , Plotly , Scikit-Learn , Machine Learning, Tensorflow , and more!', 3, 4.2, 199, 60),
(29, 'The Data Science Course 2020: Complete Data Science Bootcamp', 'Development', 'Development_03.jpg', 2589, 'Complete Data Science Training: Mathematics, Statistics, Python, Advanced Statistics in Python, Machine & Deep Learning', 3, 4.3, 239, 60),
(30, 'Deep Learning A-Z™: Hands-On Artificial Neural Networks', 'Development', 'Development_04.jpg', 3698, 'Learn to create Deep Learning Algorithms in Python from two Machine Learning & Data Science experts. Templates included.', 6, 4.3, 129, 30),
(31, 'Artificial Intelligence A-Z™: Learn How To Build An AI', 'Development', 'Development_05.jpg', 3678, 'Combine the power of Data Science, Machine Learning and Deep Learning to create powerful AI for Real-World applications!', 7, 4.2, 149, 40),
(32, 'Selenium WebDriver with Java -Basics to Advanced+Frameworks', 'Development', 'Development_06.jpg', 5596, '\"TOP RATED (BEST SELLER) #1 Master SELENIUM java course\" -5 Million students learning worldWide with great collaboration', 8, 4.2, 79, 10),
(33, 'Docker and Kubernetes: The Complete Guide', 'Development', 'Development_07.jpg', 2597, 'Build, test, and deploy Docker applications with Kubernetes while learning production-style development workflows', 4, 4.8, 199, 20),
(34, 'Flutter & Dart - The Complete Guide [2020 Edition]', 'Development', 'Development_08.jpg', 4490, 'A Complete Guide to the Flutter SDK & Flutter Framework for building native iOS and Android apps', 4, 4.2, 69, 10),
(35, 'Learn and Understand NodeJS', 'Development', 'Development_09.jpg', 8596, 'Dive deep under the hood of NodeJS. Learn V8, Express, the MEAN stack, core Javascript concepts, and more.', 2, 4.2, 499, 80),
(36, 'Ultimate AWS Certified Developer Associate 2020 - NEW!', 'Development', 'Development_10.jpg', 4693, 'Become an AWS Certified Developer! Learn all AWS Certified Developer Associate topics. PASS the Certified Developer Exam', 7, 4.6, 369, 80),
(37, 'Scrum Certification Prep +Scrum Master+ Agile Scrum Training', 'Development', 'Development_11.jpg', 4897, 'Overview of Scrum Agile project management+common questions+tips to pass PSM scrum org ONLINE Scrum Master Certification', 6, 4.3, 67, 10),
(38, 'iOS 13 & Swift 5 - The Complete iOS App Development Bootcamp', 'Development', 'Development_12.jpg', 5569, 'From Beginner to iOS App Developer with Just One Course! Fully Updated with a Comprehensive Module Dedicated to SwiftUI!', 8, 4.3, 99, 10),
(39, 'Advanced CSS and Sass: Flexbox, Grid, Animations and More!', 'Development', 'Development_13.jpg', 6988, 'The most advanced and modern CSS course on the internet: master flexbox, CSS Grid, responsive design, and so much more.', 4, 4.3, 299, 30),
(40, 'Learn Ethical Hacking From Scratch', 'IT_Software', 'Software_01.jpg', 6987, 'Become an ethical hacker that can hack computer systems like black hat hackers and secure them like security experts.', 6, 4.4, 199, 40),
(41, 'Certified Kubernetes Administrator (CKA) with Practice Tests', 'IT_Software', 'Software_04.jpg', 5570, 'Prepare for the Certified Kubernetes Administrators Certification with live practice tests right in your browser - CKA', 6, 4.3, 199, 50),
(42, 'Complete Linux Training Course to Get Your Dream IT Job 2020', 'IT_Software', 'Software_05.jpg', 7897, 'The best Linux Administration course that prepares you for corporate world and for RHCSA, RHCE and LFCS certifications', 5, 4.6, 399, 60),
(43, 'Docker for the Absolute Beginner - Hands On - DevOps', 'IT_Software', 'Software_06.jpg', 1159, 'Learn Docker with Hands On Coding Exercises. For beginners in DevOps', 7, 4.8, 99, 20),
(44, 'Cisco CCNA 200-301 – The Complete Guide to Getting Certified', 'IT_Software', 'Software_07.jpg', 3311, 'The top rated CCNA course online and only one where all questions get a response. Full lab exercises included.', 7, 4.3, 59, 10),
(45, 'AWS Certified Solutions Architect - Associate 2020', 'IT_Software', 'Software_09.jpg', 2255, 'Want to pass the AWS Solutions Architect - Associate Exam? Want to become Amazon Web Services Certified? Do this course!', 8, 4.3, 59, 10),
(46, 'AWS Certified Developer - Associate 2020', 'Development', 'Software_10.jpg', 3326, 'Do you want AWS certification? Do you want to be an AWS Certified Developer Associate? This AWS online course is for you', 6, 4.2, 149, 50),
(47, 'The Complete Digital Marketing Course - 12 Courses in 1', 'Marketing', 'Marketing_01.jpg', 1111, 'Master Digital Marketing: Strategy, Social Media Marketing, SEO, YouTube, Email, Facebook Marketing, Analytics & More!', 7, 4.4, 199, 20),
(48, 'Instagram Marketing 2020: Complete Guide To Instagram Growth', 'Marketing', 'Marketing_02.jpg', 3348, 'Attract Hyper-Targeted Instagram Followers, Convert Followers to Paying Customers, & Expand your Brand Using Instagram', 7, 4.3, 399, 50),
(49, 'Social Media Marketing MASTERY | Learn Ads on 10+ Platforms', 'Marketing', 'Marketing_03.jpg', 3368, 'MASTER online marketing on Twitter, Pinterest, Instagram, YouTube, Facebook, Google and more ad platforms! Coursenvy ™', 4, 4.3, 159, 40),
(50, 'Google Analytics Certification: Become Certified & Earn More', 'Marketing', 'Marketing_04.jpg', 3649, 'Become Google Analytics Certified to Advance Your Career, Attract Clients & Improve Your Marketing - 2020 Guide', 5, 4.6, 99, 20),
(51, 'The Complete Copywriting Course : Write to Sell Like a Pro', 'Marketing', 'Marketing_05.jpg', 3169, 'Write Effective Sales Copy & Grow Your Business & Career // Access Timeless Copywriting Formulas, Templates & FREE Tools', 5, 4.5, 99, 20),
(52, 'Mega Digital Marketing Course A-Z: 12 Courses in 1 + Updates', 'Marketing', 'Marketing_06.jpg', 1144, 'Digital Marketing Strategy, Social Media Marketing, WordPress, SEO, Digital Sale, Email, Instagram, Facebook, ads ..', 5, 4.2, 59, 60),
(53, 'How to Create Animated Videos with Powerpoint', 'Marketing', 'Marketing_07.jpg', 6699, 'Learn an easy way to create video animation with PowerPoint for business, marketing, online courses, YouTube and more', 8, 4.2, 149, 30),
(54, 'Business Branding: The Complete Course Part 1 - Strategy', 'Marketing', 'Marketing_08.jpg', 1395, 'Build A Strategic Brand From The Ground Up, To Catapult Your Marketing Impact', 7, 4.3, 69, 10),
(55, 'The Complete Facebook Marketing Masterclass', 'Marketing', 'Marketing_09.jpg', 4490, 'Dominate Facebook & Instagram, Take Your Business to the Next Level. Start Using the Complete Facebook Marketing Today!', 7, 4.3, 169, 60),
(56, 'Photography Masterclass: A Complete Guide to Photography', 'Photography', 'Photography_01.jpg', 4591, 'The Best Online Professional Photography Class: How to Take Amazing Photos for Beginners & Advanced Photographers', 8, 4.7, 199, 50),
(57, 'Affinity Photo: Solid Foundations', 'Photography', 'Photography_02.jpg', 5978, 'The best selling beginners guide to Affinity Photo - complete with a 50 page PDF to aid your study!', 4, 4.3, 199, 40),
(58, 'GIMP 2.10 Masterclass: From Beginner to Pro Photo Editing', 'Photography', 'Photography_10.jpg', 1146, 'GIMP 2.10 Complete Guide - Learn the Free Photo Editor GIMP. Go From a Beginner to Pro in Photo Editing & Graphic Design', 7, 4.3, 199, 50),
(59, 'Digital Photography for Beginners with DSLR cameras', 'Photography', 'Photography_11.jpg', 6984, 'Learn how to take stunning photographs by mastering both the technical and creative sides of digital photography.', 4, 4.6, 99, 20),
(60, 'Cinematography Course: Shoot Expert Video on Any Camera', 'Photography', 'Photography_16.jpg', 3699, 'Your Online Guide to Professional Cinematography. Learn to Shoot Stunning Video Like a Professional With Any Camera.', 2, 4.2, 59, 10);

--
-- Triggers `khoahoc`
--
DELIMITER $$
CREATE TRIGGER `add_SL_KH_GV` AFTER INSERT ON `khoahoc` FOR EACH ROW UPDATE giaovien 
SET soLuongKH = soLuongKH + 1
WHERE maGiaoVien = new.maGiaoVien
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `remove_SL_KH_GV` AFTER DELETE ON `khoahoc` FOR EACH ROW UPDATE giaovien 
SET soLuongKH = soLuongKH - 1
WHERE maGiaoVien = old.maGiaoVien
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `nguoidung`
--

CREATE TABLE `nguoidung` (
  `maNguoiDung` int(11) NOT NULL,
  `taiKhoan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `matKhau` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `hoTen` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `soDienThoai` varchar(12) COLLATE utf8_unicode_ci NOT NULL,
  `roleId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoidung`
--

INSERT INTO `nguoidung` (`maNguoiDung`, `taiKhoan`, `matKhau`, `hoTen`, `email`, `soDienThoai`, `roleId`) VALUES
(1, 'phong', 'admin', 'Tran Thanh Phong', 'phong@gmail.com', '0346251700', 2),
(2, 'phi', 'admin', 'Nguyen Phuong Phi', 'phi@gmail.com', '0393281149', 2),
(3, 'duc', 'admin', 'Nguyen Cong Duc', 'duc@gmail.com', '0359070925', 2),
(4, 'minh', 'admin', 'Nguyen Van Minh', 'minh@gmail.com', '0868702223', 2),
(5, 'nghia', 'admin', 'Truong Duc Nghia', 'nghia@gmail.com', '0866899731', 2),
(6, 'admin', 'admin', 'admin', 'admin@gmail.com', '', 3),
(7, 'huynhvien', 'admin', 'Huynh Thi Hong Vien', 'huynhvien@gmail.com', '0369455621', 1),
(8, 'tranhien', 'admin', 'Tran Thi Dieu Hien', 'tranhien@gmail.com', '03631547600', 1),
(9, 'dieulinh', 'admin', 'Nguyen thi Dieu Linh', 'dieulinh@gmail.com', '0365112522', 1),
(10, 'phuongthao', 'admin', 'Nguyen Phuong Thu Thao', 'thuthao@gmail.com', '0369145236', 1),
(11, 'phuongtu', 'admin', 'Nguyen Phuong Tu', 'phuongtu@gmail.com', '0156234522', 1),
(12, 'duchuy', 'admin', 'Truong Duc huy', 'duchuy@gmail.com', '01253621566', 1),
(13, 'tuuyen', 'admin', 'Nguyen Thao Tu Uyen', 'tuuyen@gmail.com', '0136336255', 1),
(14, 'thanhhuyen', 'admin', 'Tran Thanh Huyen', 'thanhhuyen@gmail.com', '0132225998', 1),
(15, 'thuongthuong', 'admin', 'Doan Le Thuong Thuong', 'thuongthuong@gmail.com', '013662595', 1);

-- --------------------------------------------------------

--
-- Table structure for table `nguoidungkhoahoc`
--

CREATE TABLE `nguoidungkhoahoc` (
  `maKhoaHoc` int(11) NOT NULL,
  `maNguoiDung` int(11) NOT NULL,
  `ngayDangKi` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `nguoidungkhoahoc`
--

INSERT INTO `nguoidungkhoahoc` (`maKhoaHoc`, `maNguoiDung`, `ngayDangKi`) VALUES
(2, 13, '2020-05-16'),
(8, 15, '2020-05-16'),
(11, 12, '2020-05-16'),
(12, 12, '2020-05-16'),
(12, 14, '2020-05-16'),
(14, 14, '2020-05-16'),
(18, 13, '2020-05-16'),
(19, 7, '2020-05-16'),
(20, 9, '2020-05-16'),
(21, 8, '2020-05-16'),
(22, 15, '2020-05-16'),
(23, 9, '2020-05-16'),
(23, 12, '2020-05-16'),
(23, 14, '2020-05-16'),
(24, 11, '2020-05-16'),
(26, 10, '2020-05-16'),
(27, 13, '2020-05-16'),
(30, 11, '2020-05-16'),
(34, 13, '2020-05-16'),
(39, 14, '2020-05-16'),
(41, 13, '2020-05-16'),
(42, 8, '2020-05-16'),
(42, 9, '2020-05-16'),
(46, 8, '2020-05-16'),
(49, 7, '2020-05-16'),
(50, 10, '2020-05-16'),
(53, 12, '2020-05-16'),
(55, 11, '2020-05-16'),
(56, 7, '2020-05-16'),
(56, 9, '2020-05-16'),
(58, 10, '2020-05-16'),
(60, 13, '2020-05-16');

--
-- Triggers `nguoidungkhoahoc`
--
DELIMITER $$
CREATE TRIGGER `add_SL_HV_GV` AFTER INSERT ON `nguoidungkhoahoc` FOR EACH ROW UPDATE giaovien
SET soLuongHV = soLuongHV + 1
WHERE maGiaoVien = (SELECT maGiaoVien FROM khoahoc k WHERE new.maKhoaHoc = k.maKhoaHoc )
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `remove_SL_HV` AFTER DELETE ON `nguoidungkhoahoc` FOR EACH ROW UPDATE khoahoc 
SET soLuong = soLuong - 1
WHERE maKhoaHoc = old.maKhoaHoc
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `remove_SL_HV_GV` AFTER DELETE ON `nguoidungkhoahoc` FOR EACH ROW UPDATE giaovien
SET soLuongHV = soLuongHV - 1
WHERE maGiaoVien = (SELECT maGiaoVien FROM khoahoc k WHERE old.maKhoaHoc = k.maKhoaHoc )
$$
DELIMITER ;
DELIMITER $$
CREATE TRIGGER `update_SL_KH` AFTER INSERT ON `nguoidungkhoahoc` FOR EACH ROW UPDATE khoahoc 
SET soLuong = soLuong + 1
WHERE maKhoaHoc = new.maKhoaHoc
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `danhmuc`
--
ALTER TABLE `danhmuc`
  ADD PRIMARY KEY (`maDanhMuc`);

--
-- Indexes for table `giaovien`
--
ALTER TABLE `giaovien`
  ADD PRIMARY KEY (`maGiaoVien`);

--
-- Indexes for table `khoahoc`
--
ALTER TABLE `khoahoc`
  ADD PRIMARY KEY (`maKhoaHoc`),
  ADD UNIQUE KEY `tenKhoaHoc` (`tenKhoaHoc`),
  ADD KEY `tenKhoaHoc_2` (`tenKhoaHoc`),
  ADD KEY `maDanhMuc` (`maDanhMuc`),
  ADD KEY `maGiaoVien` (`maGiaoVien`);

--
-- Indexes for table `nguoidung`
--
ALTER TABLE `nguoidung`
  ADD PRIMARY KEY (`maNguoiDung`),
  ADD UNIQUE KEY `taiKhoan` (`taiKhoan`);

--
-- Indexes for table `nguoidungkhoahoc`
--
ALTER TABLE `nguoidungkhoahoc`
  ADD PRIMARY KEY (`maKhoaHoc`,`maNguoiDung`),
  ADD KEY `maNguoiDung` (`maNguoiDung`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `giaovien`
--
ALTER TABLE `giaovien`
  MODIFY `maGiaoVien` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `khoahoc`
--
ALTER TABLE `khoahoc`
  MODIFY `maKhoaHoc` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=112;

--
-- AUTO_INCREMENT for table `nguoidung`
--
ALTER TABLE `nguoidung`
  MODIFY `maNguoiDung` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `khoahoc`
--
ALTER TABLE `khoahoc`
  ADD CONSTRAINT `khoahoc_ibfk_1` FOREIGN KEY (`maDanhMuc`) REFERENCES `danhmuc` (`maDanhMuc`),
  ADD CONSTRAINT `khoahoc_ibfk_2` FOREIGN KEY (`maGiaoVien`) REFERENCES `giaovien` (`maGiaoVien`);

--
-- Constraints for table `nguoidungkhoahoc`
--
ALTER TABLE `nguoidungkhoahoc`
  ADD CONSTRAINT `nguoidungkhoahoc_ibfk_2` FOREIGN KEY (`maNguoiDung`) REFERENCES `nguoidung` (`maNguoiDung`),
  ADD CONSTRAINT `nguoidungkhoahoc_ibfk_3` FOREIGN KEY (`maKhoaHoc`) REFERENCES `khoahoc` (`maKhoaHoc`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
